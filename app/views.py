import datetime

from django.http import Http404
from django.shortcuts import render, redirect
from django.contrib.auth import login,logout,authenticate
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.utils.timezone import make_aware

from app.models import Game, Team, Match, Member, Role
from twitch import TwitchClient



def handler404(request, *args, **argv):
    return render(request, "404.html")

def index(request):

    channelsOnline = []
    weekPlanning = [None] * 7

    week = int(datetime.datetime.now().strftime("%W"))
    year = int(datetime.datetime.now().strftime("%Y"))

    context = {
        "week" : week,
        "year" : year,
        "weekPlanning" : weekPlanning,
        "channelsOnline": channelsOnline,
    }
    return render(request, "index.html", context)
def get_planning(request):
    daypos = 0

    week = int(datetime.datetime.now().strftime("%W"))
    year = int(datetime.datetime.now().strftime("%Y"))

    if request.GET.get("week"):
        week = int(request.GET.get("week"))-1
    if request.GET.get("year"):
        year = int(request.GET.get("year"))
    if week == 52:
        week = 0
        year = year + 1
    if week == -1:
        week = 51
        year = year - 1

    weekPlanning = {"info": {"week": week, "year": year}, "data": [None] * 7}

    for x in weekPlanning["data"]:
        stringdate = '' + str(year) + ' ' + str(week) + ' ' + str(daypos)
        i = daypos - 1
        if i == -1:
            i = 6
        date = make_aware(datetime.datetime.strptime(stringdate, '%Y %W %w'))
        formateDate = date.strftime("%A %d/%m").capitalize()
        queriesDay = Match.objects.filter(date__date = date.date()).order_by('date')
        weekPlanning["data"][i] = {formateDate: []}
        for query in queriesDay:
            tmpData = {
                "game":query.game.name,
                "time": query.date.strftime("%Hh%M"),
                "team1": query.team1.name,
                "score1": query.score1,
                "team2": query.team2.name,
                "score2": query.score2,
            }
            weekPlanning["data"][i][formateDate].append(tmpData)
        daypos += 1
    return JsonResponse(weekPlanning, safe=False)

def get_streamers(request):
    clientId = 'fgplgbb3weyt567o0jql5gnyn4evey'
    channels = [
        'VALORANT_Esports_EU',
        'SkyTiiKz',
        'ONSCREEN',
        'Beardageddon',
        'ESL_CSGO'
    ]
    client = TwitchClient(client_id=clientId)
    channelIds = client.users.translate_usernames_to_ids(channels)
    onlineChannels = []
    for idObject in channelIds:
        streamInfo = client.streams.get_stream_by_user(idObject['id'])
        if streamInfo:
            onlineChannels.append(streamInfo)
    return JsonResponse(onlineChannels, safe=False)
def team(request):
    text = "Hello world"
    context = {
        "text": text
    }
    return render(request, "team.html", context)
def staff(request):
    context = {

    }
    return render(request, "staff.html", context)
def affiliate(request):

    context = {

    }
    return render(request, "affiliate.html", context)
def profil(request):
    context = {

    }
    return render(request, "profil.html", context)
def contact(request):
    context = {

    }
    return render(request, "contact.html", context)
def auth_logout(request):
    logout(request)
    context = {

    }
    return render(request, "index.html", context)
def admin(request):
    if not request.user.is_staff:
        raise Http404

    allGame = Game.objects.all()
    allTeam = Team.objects.all()
    allMatch = Match.objects.all().order_by("-date")
    users = User.objects.all()
    members = Member.objects.all()
    allUser = []
    for user in users:
        isAdded = False
        for member in members:
            if member.user == user:
                isAdded = True
                allUser.append(member)
                break
        if not isAdded:
            allUser.append(user)
    
    context = {
        "currentDate": datetime.datetime.now(),
        "allGame": allGame,
        "allTeam": allTeam,
        "allMatch": allMatch,
        "allUser": allUser,
    }
    return render(request, "admin.html", context)
def add_game(request):
    if not request.user.is_staff:
        raise Http404

    if request.POST.get('gameName'):
        gameName = request.POST.get('gameName')
        game = Game(name=gameName)
        game.save()
    else:
        print("error: Game was not created")

    return redirect('/admin')
def delete_game(request):
    if not request.user.is_staff:
        raise Http404

    if request.GET.get('gamePk'):
        gamePk = request.GET.get('gamePk')
        Game.objects.get(id=gamePk).delete()
    else:
        print("error: Game was not deleted")

    return redirect('/admin')
def add_team(request):
    if not request.user.is_staff:
        raise Http404

    if request.POST.get('teamName'):
        teamName = request.POST.get('teamName')
        teamStructure = request.POST.get('teamStructure')
        teamGame = Game.objects.get(pk=request.POST.get('teamGame'))
        team = Team(name=teamName, structure=teamStructure, game=teamGame)
        team.save()
    else:
        print("error: Team was not created")

    return redirect('/admin')
def delete_team(request):
    if not request.user.is_staff:
        raise Http404

    if request.GET.get('teamPk'):
        teamPk = request.GET.get('teamPk')
        Team.objects.get(id=teamPk).delete()
    else:
        print("error: Team was not deleted")

    return redirect('/admin')
def add_match(request):
    if not request.user.is_staff:
        raise Http404

    if request.POST.get('team1Match'):
        team1Match = Team.objects.get(pk=request.POST.get('team1Match'))
        team2Match = Team.objects.get(pk=request.POST.get('team2Match'))
        score1Match = None
        score2Match = None
        if request.POST.get('score1Match') or request.POST.get('score2Match'):
            score1Match = request.POST.get('score1Match')
            score2Match = request.POST.get('score2Match')
        gameMatch = Game.objects.get(pk=request.POST.get('gameMatch'))
        dateMatch = datetime.datetime.strptime(request.POST.get('dateMatch'),"%d/%m/%Y %H:%M")

        match = Match(team1=team1Match, score1=score1Match, team2=team2Match, score2=score2Match, game=gameMatch, date=dateMatch)
        match.save()
    else:
        print("error: Team was not created")

    return redirect('/admin')
def set_match(request):
    if not request.user.is_staff:
        raise Http404

    if request.POST.get('team1Match'):
        team1Match = Team.objects.get(pk=request.POST.get('team1Match'))
        team2Match = Team.objects.get(pk=request.POST.get('team2Match'))
        score1Match = None
        score2Match = None
        if request.POST.get('score1Match') or request.POST.get('score2Match'):
            score1Match = request.POST.get('score1Match')
            score2Match = request.POST.get('score2Match')
        gameMatch = Game.objects.get(pk=request.POST.get('gameMatch'))
        dateMatch = datetime.datetime.strptime(request.POST.get('dateMatch'),"%d/%m/%Y %H:%M")

        match = Match(team1=team1Match, score1=score1Match, team2=team2Match, score2=score2Match, game=gameMatch, date=dateMatch)
        match.save()
    else:
        print("error: Team was not created")

    return redirect('/admin')
def delete_match(request):
    if not request.user.is_staff:
        raise Http404

    if request.GET.get('matchPk'):
        matchPk = request.GET.get('matchPk')
        Match.objects.get(id=matchPk).delete()
    else:
        print("error: Match was not deleted")

    return redirect('/admin')